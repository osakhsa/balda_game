# -*- coding: utf-8 -*-

from os import getenv
import random
import re

# noinspection PyPackageRequirements
import telebot

# noinspection PyPackageRequirements
from telebot import types
# noinspection PyPackageRequirements
from telebot.callback_data import CallbackData
from requests import get, Response
# import requests

# Токен по умолчанию берётся из переменной среды, но на всякий случай сохранён в файле (файл в.gitignore)
TOKEN = getenv("BOT_TOKEN")
if TOKEN is None:
    try:
        with open("bot_token.txt", encoding='utf-8') as token_file:
            TOKEN = token_file.read()
    except FileNotFoundError:
        print("""Нет токена. Вероятно, у вас нет права запускать этот бот.
Разработчикам: убедитесь, что токен загружен в переменную окружения BOT_TOKEN или
находится в файле bot_token.txt (кодировка utf-8) в корневой директории проекта.""")
        raise SystemExit(1)

WORDLIST_FILE = "леммы с абрикосами.txt"
with open(WORDLIST_FILE, encoding="utf-8") as wordlist_file:
    WORDLIST = [a.strip() for a in wordlist_file.readlines()]

bot = telebot.TeleBot(TOKEN, skip_pending=True)
sequence_cf = CallbackData('sequence', prefix="hint")  # Шаблон для сообщений кнопки "Подсказка"
scores = {}  # Словарь, в котором хранятся результаты текущей игры для каждого юзера (числа) и финишные слова
GENERAL_FINAL_WORD = "БАЛДА"  # Финишное слово по умолчанию
random.seed()  # Инициализация рандом айзера

# Кнопка новой игры
new_game_markup = telebot.types.InlineKeyboardMarkup()
new_game_button = telebot.types.InlineKeyboardButton("Новая игра", callback_data="new game")
new_game_markup.add(new_game_button)
# клавиатура для подтверждения завершения игры
finish_markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
finish_button = types.KeyboardButton("Да")
not_finish_button = types.KeyboardButton("Нет")
finish_markup.add(finish_button, not_finish_button)


def end_round(user_id, loser):  # Вызывается после окончания раунда по любым причинам
    """Завершает раунды и игры."""
    scores[user_id][loser] += 1  # +1 штрафное очко
    bot.send_message(user_id, f'''\
Ваш счёт: {scores[user_id]["goal"][:scores[user_id]["user"]]}
Счёт бота: {scores[user_id]["goal"][:scores[user_id]["bot"]]}\
''')  # Печатаем первые х букв финишного слова этой игры для обоих игроков
    if scores[user_id][loser] == len(scores[user_id]["goal"]):  # Если кто-то только что обалдел
        scores[user_id]["playing"] = False
        if loser == "user":
            bot.send_message(user_id, """Поздравляю, вы балда!
Может, сыграем ещё раз?""", reply_markup=new_game_markup)
        elif loser == "bot":
            bot.send_message(user_id, """Ура, вы выиграли!
Если хотите, можем сыграть ещё раз""", reply_markup=new_game_markup)
    else:
        start_round(user_id, first_player=loser)


# Доработать в подсказках:
# - придумать и написать подтверждение взятия подсказки;
# - придумать и написать более умный алгоритм выбора target.
@bot.callback_query_handler(func=lambda call: call.data.startswith("hint"))
def provide_hint(call):
    """Выбирает слово для подсказки и предоставляет подсказку игроку."""
    if scores[call.from_user.id]["hints_left"] == 0:  # Такое может случиться, если взять несколько подсказок сразу
        bot.answer_callback_query(call.id, "Вы исчерпали лимит подсказок за эту партию :-((")
    else:
        callback_data: dict = sequence_cf.parse(callback_data=call.data)
        hint_sequence = callback_data["sequence"]
        suitable_words = list(filter(lambda word: hint_sequence in word, WORDLIST))
        target = random.choice(suitable_words)  # Слово, которое будет подсказывать бот (в случайную пользу)
        sequence_position = (target.index(hint_sequence), target.index(hint_sequence) + len(hint_sequence))
        hint_text = "*"*sequence_position[0] + hint_sequence + "*"*(len(target) - sequence_position[1])
        bot.send_message(call.from_user.id, hint_text)  # В hint_text звёздочки вместо неоткрытых букв target
        scores[call.from_user.id]["hints_left"] -= 1
        bot.send_message(call.from_user.id, f'''Осталось подсказок: {scores[call.from_user.id]["hints_left"]}
Ваша буква:''')


def person_turn_start(user_id, sequence: str, first=False):
    """Начало хода пользователя, получение буквы."""
    person_turn_markup = telebot.types.InlineKeyboardMarkup()
    if scores[user_id]["hints_left"] > 0:
        hint_button = telebot.types.InlineKeyboardButton("Подсказка", callback_data=sequence_cf.new(sequence))
        person_turn_markup.add(hint_button)
    start_message = bot.send_message(user_id, 'Ваша буква: ', reply_markup=(None if first else person_turn_markup))
    bot.register_next_step_handler(start_message, person_turn_mid, start_message.id, sequence, first)


def person_turn_mid(input_message, ask_id, sequence: str, first=False):
    # Проверка на корректность ввода пользователя + дальше передается не весь message,
    # а лишь message.chat.id и message.text
    # Удаляем кнопку подсказки из прошлого сообщения после того, как сделан ход. 
    # Если её и не было – ничего не делаем
    try:
        bot.edit_message_reply_markup(chat_id=input_message.chat.id, message_id=ask_id,
                                      reply_markup=None)
    except telebot.apihelper.ApiTelegramException:
        pass
    user_letter = input_message.text.lower().strip()
    if user_letter in command_handlers.keys():  # если только что ввели команду, вызываем её обработчик
        if user_letter in ["/finish"]:  # этой команде надо передать параметры
            command_handlers[user_letter](input_message, person_turn_start, sequence, first)
        else:
            command_handlers[user_letter](input_message)
        if user_letter not in ["/start", "/finish"]:  # После этих команд не надо (отдельно) регистрировать ход
            bot.send_message(input_message.chat.id, f'Мы остановились на: -{sequence}-')
            person_turn_start(input_message.chat.id, sequence, first)
    elif user_letter == "ё":
        bot.send_message(input_message.chat.id, 'В этой версии игры "е" и "ё" не различаются. '
                                                'Буква была автоматически заменена на "е".')
        person_turn_begin_or_end(input_message.chat.id, "е", sequence, first)
    elif re.fullmatch(r'[а-яё]', user_letter) is not None:
        person_turn_begin_or_end(input_message.chat.id, user_letter, sequence, first)
    else:
        bot.send_message(input_message.chat.id,
                         "Вы ввели некорректный символ :( \n"
                         "Пожалуйста, введите одну из букв кириллического алфавита")
        person_turn_start(input_message.chat.id, sequence, first)


def person_turn_begin_or_end(user_id, letter: str, sequence: str, first=False):
    """Предлагает пользователю выбрать, с какой стороны приписать букву."""
    if first:
        person_turn_final(None, user_id, letter, sequence, first)
    else:
        # Создаются Reply кнопки + далее передается новый message из них (message.text: в начало/в конец)
        begin_or_end_markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        btn_begin = types.KeyboardButton('в начало')
        btn_end = types.KeyboardButton('в конец')
        begin_or_end_markup.add(btn_begin, btn_end)
        begin_or_end = bot.send_message(user_id, text='Ваша буква пойдет: ', reply_markup=begin_or_end_markup)
        bot.register_next_step_handler(begin_or_end, person_turn_final, user_id, letter, sequence, first)


def person_turn_final(begin_or_end, user_id, letter: str, sequence: str, first=False):
    """Обработка нажатия на кнопки "В начало" и "В конец". Получение результата хода."""
    user_letter = letter.lower()
    if first:
        sequence = user_letter
        final_to_bot_turn(user_id, sequence)
    else:
        if begin_or_end.text == 'в начало':
            sequence = user_letter + sequence
            bot.send_message(user_id, "Ваш ход: " + sequence, reply_markup=None)
            final_to_bot_turn(user_id, sequence)
        elif begin_or_end.text == 'в конец':
            sequence += user_letter
            bot.send_message(user_id, "Ваш ход: " + sequence, reply_markup=None)
            final_to_bot_turn(user_id, sequence)
        else:
            # Игрок не может в момент выбора куда поставить букву прислать ничего кроме выбора буквы
            bot.send_message(user_id,
                             "Простите, не понимаю что Вы хотите сказать :( \n"
                             "Пожалуйста, нажмите на одну из кнопок на дополнительной клавиатуре: "
                             "'в начало' или 'в конец'", reply_markup=None)
            # person_turn_start(user_id, sequence, first)
            person_turn_begin_or_end(user_id, user_letter, sequence, first)


def final_to_bot_turn(user_id, sequence):
    """Проверка последовательности после хода пользователя на слово и блеф. Переход хода."""
    if sequence in WORDLIST:
        bot.send_message(user_id, sequence + " – слово. Кажется, вы проиграли раунд. Давайте начнем новый!")
        end_round(user_id, loser="user")
    elif re.search(sequence, " ".join(WORDLIST)) is None:
        suspicion(user_id, sequence)
    else:
        bot_turn(user_id, sequence)


def suspicion(user_id, sequence):
    doubt_message = bot.send_message(user_id, f'Я не могу вспомнить слово с буквосочетанием "{sequence}". \
Скажите, что вы имели в виду – если такое слово найдётся в Викисловаре и подходит под условия, \
вы выиграли раунд.\nЕсли в слове есть буква "ё", пожалуйста, НЕ заменяйте её на "е".')
    bot.register_next_step_handler(doubt_message, unknown_word, sequence)


def unknown_word(user_word_message, sequence):
    user_word = user_word_message.text.lower().strip()
    if user_word in command_handlers.keys():  # если только что ввели команду, вызываем её обработчик
        if user_word in ["/finish"]:  # этой команде надо передать параметры
            command_handlers[user_word](user_word_message, suspicion, sequence)
        else:
            command_handlers[user_word](user_word_message)
        if user_word not in ["/start", "/finish"]:  # после этих команд не надо (отдельно) возвращаться к игре
            suspicion(user_word_message.chat.id, sequence)
    elif re.fullmatch(r'[а-яё]*', user_word) is None:
        bot.send_message(user_word_message.chat.id, "В слове могут быть только буквы русского алфавита. "
                                                    "Не считается. Вы проиграли раунд.")
        end_round(user_word_message.chat.id, loser="user")
    elif sequence not in user_word:
        bot.send_message(user_word_message.chat.id, f'Последовательности "{sequence}" нет в слове "{user_word}". '
                                                    'Не считается. Вы проиграли раунд.')
        end_round(user_word_message.chat.id, loser="user")
    else:
        # noinspection SpellCheckingInspection
        wiktionary_request: Response = get("https://ru.wiktionary.org/w/api.php", params={
            'action': 'parse', 'format': 'json',  # бот обращается к API Викисловаря и ищет в нём слово
            'page': user_word, 'prop': 'categories',  # в categories есть информация о части речи и "собственности"
            'formatversion': 2})  # не знаю, что это, но браузер автоматически дописывает
        if wiktionary_request.status_code != 200:
            bot.send_message(user_word_message.chat.id, 'Викисловарь почему-то недоступен... Поверю, что '
                                                        f'слово "{user_word}" есть. Вы выиграли раунд.')
            end_round(user_word_message.chat.id, loser="bot")
        else:
            wiktionary_json = wiktionary_request.json()
            if wiktionary_json.get("error"):  # страницы не существует
                bot.send_message(user_word_message.chat.id,
                                 f'Увы, слова "{user_word}" нет в Викисловаре. '
                                 'Не получилось. Вы проиграли раунд.')
                end_round(user_word_message.chat.id, loser="user")
            elif wiktionary_json.get("parse"):  # страница существует! и её удалось распарсить
                categories_gen = (i["category"] for i in wiktionary_json["parse"]["categories"])
                if 'Русские_существительные' not in categories_gen:
                    bot.send_message(user_word_message.chat.id,
                                     f'Слово "{user_word}" есть в Викисловаре, но это не русское существительное. '
                                     'Не получилось. Вы проиграли раунд.')
                    end_round(user_word_message.chat.id, loser="user")
                else:  # Проверять, что не имя собственное, необязательно из-за регистра букв
                    bot.send_message(user_word_message.chat.id,
                                     f'Да! Слово "{user_word}" есть в Викисловаре и удовлетворяет правилам игры. '
                                     'Ура, вы выиграли раунд!')
                    end_round(user_word_message.chat.id, loser="bot")


def bot_turn(user_id, sequence: str, first=False):
    """Осуществляет ход бота от начала до конца, включая проверку на слово результата."""
    if first:  # Абсолютно первым ходом выбираем случайную букву случайного слова.
        target = random.choice(WORDLIST)
        sequence = random.choice(target)
    else:
        suitable_words = list(filter(lambda word: sequence in word, WORDLIST))  # Все слова с нужными буквами
        good_words = list(filter(lambda word: (len(word) - len(sequence)) % 2 == 0, suitable_words))  # Выигрышные
        if not good_words:
            good_words = suitable_words  # Если есть хоть одно слово нужной длины, выбираем из них, иначе из всех
        target = random.choice(good_words)  # Выбираем случайное слово, ищем в нём последовательность
        sequence_position = (target.index(sequence), target.index(sequence) + len(sequence))
        if sequence_position[0] == 0:  # В начале – добавим букву справа, в конце – слева, в середине – случайно
            from_right = True
        elif sequence_position[1] == len(target):
            from_right = False
        else:
            from_right = random.choice((True, False))
        if from_right:  # Берём срез на один символ шире
            sequence = target[sequence_position[0]:sequence_position[1] + 1]
        else:
            sequence = target[sequence_position[0] - 1:sequence_position[1]]
        # print(suitable_words, good_words, target, sequence_position, sequence)
    bot.send_message(user_id, "Мой ход: " + sequence)
    if sequence in WORDLIST:
        bot.send_message(user_id, sequence + " – слово. Поздравляем, вы выиграли раунд. \
Давайте начнем новый!")
        end_round(user_id, loser="bot")
    elif re.search(sequence, " ".join(WORDLIST)) is None:
        bot.send_message(user_id,
                         sequence + " - мы не нашли слова с подобным сочетанием букв. "
                                    "Кажется, бот блефовал. Поздравляем с победой в раунде!.")
        end_round(user_id, loser="bot")
    else:
        person_turn_start(user_id, sequence)


def start_round(user_id, first_player, first_round=False):
    """Запускает первый ход раунда."""
    # Все раунды, кроме первого, начинает проигравший предыдущий раунд.
    # Первый раунд – рандомно
    bot.send_message(user_id, "Начинаем раунд!")
    if first_round:
        if random.randrange(2):  # Случайное число из 0 и 1, то есть случайно True или False
            person_turn_start(user_id, sequence='', first=True)
        else:
            bot_turn(user_id=user_id, sequence='', first=True)
    elif first_player == 'bot':
        bot_turn(user_id=user_id, sequence='', first=True)
    else:
        person_turn_start(user_id, sequence='', first=True)


# Эта функция вызывается по кнопке "Новая игра"
@bot.callback_query_handler(func=lambda call: call.data == "new game")
def start_game(call):
    """Сбрасывает счёт и запускает первый раунд новой игры."""
    bot.send_message(call.from_user.id, """\
                     Ура, новая игра!\
                     """)
    scores[call.from_user.id] = {"user": 0, "bot": 0, "goal": GENERAL_FINAL_WORD,
                                 "hints_left": 2, "playing": True}  # сбрасываем счёт
    start_round(user_id=call.from_user.id, first_player="", first_round=True)


# Это обработчик встроенной команды /help, здесь должна быть инструкция, как пользоваться ботом
@bot.message_handler(commands=['help'])
def instruction(message):
    """Высылает пользователю сообщение со списком доступных команд после команды."""
    bot.send_message(message.chat.id, """\
Доступные команды:
/help — инструкция по работе с ботом
/rules — показать правила игры
/start — начать новую игру
/example — пример хода игры
/finish — завершить игру
\
""")


# Бот должен написать правила игры после вызова команды /rules
@bot.message_handler(commands=['rules'])
def send_rules(message):
    """Высылает пользователю правила игры после команды."""
    bot.send_message(message.chat.id, '''\
Правила игры:
Игроком выбирается любая первая буква. \
После этого игроки по очереди в свой ход приписывают букву справа или слева, \
желая дополнить сочетание до любого допустимого слова.
Цель игры — не стать игроком, \
который будет вынужден дополнить сочетание до существующего слова. \
Когда это происходит, игрок проигрывает раунд \
и получает букву из слова БАЛДА "на счёт". \
Начинается новый кон. Игра завершается, \
когда кто-то соберёт "на счету" всё слово целиком, \
а значит станет балдой.
В случае, если игрок не понимает, \
какое слово может существовать с таким сочетанием букв, \
он может усомниться в существовании слова. В таком случае он узнаёт у оппонента, \
к какому слову тот вёл. Если названное слово существует, проигрывает усомнившийся; \
если же такого слова нет – проигрывает его оппонент.
Правила выбора слов:
• используются существительные в начальной форме;
• нельзя использовать названия — имена собственные;
• не используются слова, содержащие дефис;
• "е" и "ё" считаются одной буквой.
За игру можно взять две подсказки. Подсказка сообщает, \
по сколько букв слева и справа можно добавить, чтобы \
получилось какое-нибудь существующее слово, и выглядит так: **рико*. \
Следовать взятой подсказке необязательно.
Пример хода игры по команде /example
\
''')


# Эта функция запустится по команде /start. Кратчайшее введение про help и rules и кнопка новой игры
@bot.message_handler(commands=['start'])
def send_start_message(message):
    """Высылает пользователю стартовое сообщение и кнопку новой игры после команды."""
    bot.send_message(message.chat.id, """\
Привет! Я — бот, созданный для игры в Балду.
Балда — это игра, в которой требуется составить слово, \
добавляя по одной букве справа или слева. \
Ваша задача — не стать тем, кому придётся закончить слово :)
/help — инструкция по работе с ботом
/rules — правила игры
Начать игру:
\
""", reply_markup=new_game_markup)


def finish(finish_message, func, *args):  # func – функция, которую нужно вызвать при продолжении игры
    """Обрабатывает ответ на вопрос, точно ли пользователь хочет завершить игру."""
    if finish_message.text == "Да":
        scores[finish_message.chat.id]["playing"] = False
        bot.send_message(finish_message.chat.id, text="Ох, жаль что игра закончилась. "
                                                      "Приходите поиграть снова!", reply_markup=new_game_markup)
    elif finish_message.text == "Нет":
        bot.send_message(finish_message.chat.id, text="Отлично! Тогда продолжаем", reply_markup=None)
        func(finish_message.chat.id, *args)
    else:
        bot.send_message(finish_message.chat.id, "Простите, не понимаю что Вы хотите сказать :( \n "
                                                 "Пожалуйста,"
                                                 "нажмите на одну из кнопок на дополнительной клавиатуре: "
                                                 "'да' или 'нет'", reply_markup=None)
        finish_approve(finish_message, func, *args)


@bot.message_handler(commands=['finish'])
def finish_approve(message, func=None, *args):
    """Уточняет, хочет ли и может ли пользователь завершить игру, после команды."""
    try:
        if scores[message.chat.id]["playing"]:
            finish_message_person = bot.send_message(message.chat.id, text="Ой. "
                                                                           "Вы точно хотите завершить игру?",
                                                     reply_markup=finish_markup)
            bot.register_next_step_handler(finish_message_person, finish, func, *args)
        else:
            bot.send_message(message.chat.id, text="Ой. Вы не можете завершить игру, которой нет. "
                                                   "Начните играть и "
                                                   "снова воспользуйтесь командой /finish "
                                                   "если захотите закончить игру",
                             reply_markup=new_game_markup)
    except KeyError:  # Если человек ещё не начинал играть и его нет в scores
        bot.send_message(message.chat.id, text="Вы ещё не играли с ботом. Для начала работы нажмите /start")


@bot.message_handler(commands=['example'])
def send_example(message):
    """Высылает пользователю пример игры после команды."""
    bot.send_message(message.chat.id, """\
Ход игры:
1) Счёт 0:0. Первый игрок выбирает букву "А":
_А_
2) Второй хочет, чтобы получилось слово "ДАР", \
поэтому ставит букву "Р" справа от "А":
_АР_
3) Первый игрок не хочет проигрывать, \
поэтому решает вести до слова "МАРТ" \
и ставит букву "М" перед "АР":
_МАР_
4) Второй игрок вспоминает, что есть слово "КОМАР", \
поэтому ставит "О" перед "МАР":
_ОМАР_
И проигрывает. \
Так как слово "ОМАР" существует самостоятельно в русском языке.
5) Теперь счёт 0:Б (первая буква от "БАЛДА"); \
начинается второй раунд.
\
""")


@bot.message_handler(func=lambda message: True)
def starting_dialog(message):
    """Перехватывает сообщения в перерывах между играми и предлагает рассказать факт."""
    bot.reply_to(message, 'Привет!')
    dialog_markup = telebot.types.InlineKeyboardMarkup()
    dialog_markup.add(telebot.types.InlineKeyboardButton('Да, давай', callback_data='yes'),
                      telebot.types.InlineKeyboardButton('Нет', callback_data='no'))
    bot.send_message(message.chat.id, 'Могу рассказать интересный факт, если не хочешь играть сейчас.',
                     reply_markup=dialog_markup)


@bot.callback_query_handler(func=lambda call: call.data == "yes")
def tell_fact(call):
    """Рассказывает факт, если пользователь согласился (см. starting_dialog)."""
    bot.edit_message_reply_markup(chat_id=call.from_user.id, message_id=call.message.id, reply_markup=None)
    bot.send_message(call.from_user.id, """\
    Почему абрикосы?
Как ты можешь заметить, на аватарке у меня абрикосы. Возникла эта идея случайно: в ходе разработки тестового \
словника для игры одна из разработчиц проекта сделала ошибку в коде, который собирал леммы для словника, в \
результате чего была потеряна половина слов, в том числе слово "абрикос". Благодаря проверке наличия "абрикосов"\
 в словнике ошибка была исправлена, и верный файл получил название "леммы с абрикосами". Вот и повелось...
Если хочешь начать новую игру, это можно сделать по команде /start\
    """, reply_markup=None)


@bot.callback_query_handler(func=lambda call: call.data == "no")
def quit_dialog(call):
    """Завершает диалог, если пользователю не интересен факт (см. starting_dialog)."""
    bot.edit_message_reply_markup(chat_id=call.from_user.id, message_id=call.message.id, reply_markup=None)
    bot.send_message(call.from_user.id, """\
    Хорошо! Увидимся, как захочешь поиграть :)
Начать новую игру можно по команде /start
    \
    """, reply_markup=None)


command_handlers = {'/start': send_start_message, '/help': instruction,
                    '/rules': send_rules, '/example': send_example,
                    '/finish': finish_approve, }  # Все используемые команды и их обработчики

if __name__ == "__main__":
    bot.infinity_polling()
